import * as PIXI from "pixi.js";
import MultiStyleText from "pixi-multistyle-text";

let Container = PIXI.Container;
let autoDetectRenderer = PIXI.autoDetectRenderer;
let Graphics = PIXI.Graphics;
let Text = PIXI.Text;
let Polygon = PIXI.Polygon;

const FLAT = true;
const POINTY = false;
const OFFSET = true;
const AXIAL = false;

const newStage = (x, y) => {
    let canvas = document.querySelector("canvas")
    if (canvas) {
        canvas.remove();
    }
    let stage = new Container();
    let renderer = autoDetectRenderer(x, y, {antialias: true});
    document.querySelector(".stage").appendChild(renderer.view);

    renderer.view.style.border = "1px dashed black";
    renderer.backgroundColor = 0xFFEECC;
    stage.render = () => {
        renderer.render(stage);
    }
    stage.render();
    return stage;
}
const hex_corner = (center, size, i, flatTop) => {
    var angle_deg = 60 * i   + (flatTop ? 0 : 30)
    var angle_rad = Math.PI / 180 * angle_deg
    return [center.x + size * Math.cos(angle_rad),
                 center.y + size * Math.sin(angle_rad)]
}

const getXYHexagon = (gridOffset, x, y, flatTop = true, offset = true) => {
    let hexagon = new Graphics();
    hexagon.beginFill(0xFFFFFF);
    // g.beginFill(0xFF9933);
    hexagon.lineStyle(1, 0x006600, 1);

    let size = 30;
    let width = size * 2;
    let height = Math.sqrt(3)/2 * width / 2;
    let horiz = (width * 3/4) / 2;
    let vert = height;

    if (!flatTop) {
        height = size * 2;
        width = Math.sqrt(3)/2 * height / 2;
        horiz = width;
        vert = (height * 3/4) / 2;
    }

    let yOffset = 0;
    let xOffset = 0;
    if (offset) {
        if (x % 2 !== 0 && flatTop) {
            yOffset = (vert / 2);
        }
        if (y % 2 !== 0 && !flatTop) {
            xOffset = (horiz / 2);
        }
    } else {
        if (flatTop) {
            yOffset = x * vert / 2;
        } else {
            xOffset = y * horiz / 2;
        }
    }

    hexagon.index = {x, y}
    hexagon.x = gridOffset.x + (x * horiz) + xOffset;
    hexagon.y = gridOffset.y + (y * vert) + yOffset;

    let points = [];
    for (let i = 0; i <= 6; i++) {
        points.push(...hex_corner(hexagon,size,i, flatTop));
    };
    hexagon.drawPolygon(points)
    hexagon.endFill();

    hexagon.hitArea = new Polygon(points);
    hexagon.interactive = true;
    hexagon.click = () => {
        console.log(`MOUSE CLICK ${hexagon.index.x},${hexagon.index.y}`);
    }

    let message = new MultiStyleText(
        "",
        { "default": {fontSize: "12px", fontFamily: "Arial", fill: "#000000"}, "xb": { fill: "#59b102", fontWeight: "bold"}, "yb": { fill: "#0099e6", fontWeight: "bold"} }
    );
    hexagon.message = message;
    hexagon.addChild(message);
    hexagon.addMessage = (msg) => {
        hexagon.message.text = msg;
        hexagon.message.x = hexagon.x - (message.width / 2);
        hexagon.message.y = hexagon.y - (message.height / 2);
    }
    return hexagon;
}

const reRenderXYHexes = (stage) => {
    stage.hexes.forEach((hex) => {
        hex.tint = "0xFFFFFF";
        let xMsg = hex.index.x;
        if (stage.selected.x == hex.index.x) {
            xMsg = `<xb>${hex.index.x}</xb>`;
            hex.tint = "0xe2e7e9";
        }
        let yMsg = hex.index.y;
        if (stage.selected.y == hex.index.y) {
            yMsg = `<yb>${hex.index.y}</yb>`;
            hex.tint = "0xdee4d7";
        }
        if (stage.selected.x == hex.index.x && stage.selected.y == hex.index.y) {
            hex.tint = "0xc3d7d0";
        }
        hex.addMessage(`${xMsg},${yMsg}`);
    })
    stage.render();
}

let title = document.querySelector("#title");

window.stage1 = () => {
    let stage1 = newStage(200, 200);
    let hex = getXYHexagon({x:50, y:50}, 0, 0, FLAT);
    hex.addMessage(`0,0`);
    stage1.addChild(hex);
    stage1.render();
    title.innerText = "Flat Top Hexagon";
}
window.stage1();

window.stage2 = () => {
    let stage2 = newStage(200, 200);
    let hex2 = getXYHexagon({x:50, y:50}, 0, 0, POINTY);
    hex2.addMessage(`0,0`);
    stage2.addChild(hex2);
    stage2.render();
    title.innerText = "Pointy Top Hexagon";
};

window.stage3 = () => {
    let stage3 = newStage(200, 200);
    for (let x = 0; x<= 2; x++) {
        for (let y = 0; y<= 2; y++) {
            let hex = getXYHexagon({x:25, y:20}, x, y, FLAT);
            hex.addMessage(`${x},${y}`);
            stage3.addChild(hex);
        }
    }
    stage3.render();
    title.innerText = "Flat Top Offset Grid (3x3)";
};

window.stage4 =  () => {
    let stage4 = newStage(200, 200);
    for (let x = 0; x<= 2; x++) {
        for (let y = 0; y<= 2; y++) {
            let hex = getXYHexagon({x:20, y:25}, x, y, POINTY);
            hex.addMessage(`${x},${y}`);
            stage4.addChild(hex);
        }
    }
    stage4.render();
    title.innerText = "Pointy Top Offset Grid (3x3)";
}

window.stage5 = () => {
    let stage5 = newStage(350, 450);
    stage5.selected = {}
    stage5.hexes = [];
    for (let x = 0; x<= 6; x++) {
        for (let y = 0; y<= 6; y++) {
            let hex = getXYHexagon({x:20, y:20}, x, y, FLAT, OFFSET);
            hex.addMessage(`${x},${y}`);
            hex.mouseover = (mouseData) => {       stage5.selected = hex.index; reRenderXYHexes(stage5)    }
            stage5.hexes.push(hex);
            stage5.addChild(hex);
        }
    }
    stage5.render();
    title.innerHTML = "Flat Top Offset Grid (7x7) <span>With Hover States</span>";
}

window.stage6 = () => {
    let stage6 = newStage(425, 375);
    stage6.selected = {}
    stage6.hexes = [];
    for (let x = 0; x<= 6; x++) {
        for (let y = 0; y<= 6; y++) {
            let hex = getXYHexagon({x:20, y:20}, x, y, POINTY, OFFSET);
            hex.addMessage(`${x},${y}`);
            hex.mouseover = (mouseData) => {       stage6.selected = hex.index; reRenderXYHexes(stage6)    }
            stage6.hexes.push(hex);
            stage6.addChild(hex);
        }
    }
    stage6.render();
    title.innerHTML = "Pointy Top Offset Grid (7x7) <span>With Hover States</span>";
}

window.stage7 = () => {
    let stage7 = newStage(375, 400);
    stage7.selected = {}
    stage7.hexes = [];
    stage7.points = [];
    stage7.points[-3] = [0, 1, 2, 3];
    stage7.points[-2] = [-1, 0, 1, 2, 3];
    stage7.points[-1] = [-2, -1, 0, 1, 2, 3];
    stage7.points[0] = [-3, -2, -1, 0, 1, 2, 3];
    stage7.points[1] = [-3, -2, -1, 0, 1, 2];
    stage7.points[2] = [-3, -2, -1, 0, 1];
    stage7.points[3] = [-3, -2, -1, 0];
    for (let x = -3; x<= 3; x++) {
        let points = stage7.points[x];
        points.forEach((y) => {
            let hex = getXYHexagon({x:90, y:100}, x, y, FLAT, AXIAL);
            hex.addMessage(`${x},${y}`);
            hex.mouseover = (mouseData) => { stage7.selected = hex.index; reRenderXYHexes(stage7)    }
            stage7.hexes.push(hex);
            stage7.addChild(hex);
        });
    }
    stage7.render();
    title.innerHTML = "Flat Top Axial Grid<span>With Hover States</span>";
}

window.stage8 = () => {
    let stage8 = newStage(400, 350);
    stage8.selected = {}
    stage8.hexes = [];
    stage8.points = [];
    stage8.points[-3] = [0, 1, 2, 3];
    stage8.points[-2] = [-1, 0, 1, 2, 3];
    stage8.points[-1] = [-2, -1, 0, 1, 2, 3];
    stage8.points[0] = [-3, -2, -1, 0, 1, 2, 3];
    stage8.points[1] = [-3, -2, -1, 0, 1, 2];
    stage8.points[2] = [-3, -2, -1, 0, 1];
    stage8.points[3] = [-3, -2, -1, 0];
    for (let x = -3; x<= 3; x++) {
        let points = stage8.points[x];
        points.forEach((y) => {
            let hex = getXYHexagon({x:100, y:90}, x, y, POINTY, AXIAL);
            hex.addMessage(`${x},${y}`);
            hex.mouseover = (mouseData) => { stage8.selected = hex.index; reRenderXYHexes(stage8) }
            stage8.hexes.push(hex);
            stage8.addChild(hex);
        });
    }
    stage8.render();
    title.innerHTML = "Pointy Top Axial Grid<span>With Hover States</span>";
}

const getHexFromCoordinates = (hexes, x, y) => {
    let filteredHex = hexes.filter((hex) => {
        return hex.index.x == x && hex.index.y == y;
    });
    if (filteredHex) {
        return filteredHex[0];
    }
}

const axial_directions = [
    { x: +1, y: 0}, { x: +1, y: -1}, { x: 0, y: -1},
    { x: -1, y: 0}, { x:-1, y: +1}, { x:0, y: +1},
];

const getHexFromDirection = (stage, selectedHex, direction) => {
    let x = selectedHex.index.x + direction.x;
    let y = selectedHex.index.y + direction.y;
    return getHexFromCoordinates(stage.hexes, x, y);
}

var axial_diagonals = [
    { x: +2, y: -1 }, { x: +1, y: +1 }, { x: -1, y: +2 } ,
    { x: -2, y: +1 }, { x: -1, y: -1 }, { x: +1, y: -2 }
]

const reRenderWithDirections = (stage, directions) => {
    stage.hexes.forEach((hex) => {
        hex.tint = "0xFFFFFF";
        let xMsg = hex.index.x;
        let yMsg = hex.index.y;
        hex.addMessage(`${xMsg},${yMsg}`);
    })
    let selectedHex = stage.selectedHex;
    selectedHex.addMessage(`<xb>${selectedHex.index.x}</xb>,<yb>${selectedHex.index.y}</yb>`);
    selectedHex.tint = "0xc3d7d0";
    directions.forEach((direction) => {
        let hexNeighbor = getHexFromDirection(stage, selectedHex, direction);
        console.log(hexNeighbor);
        if (hexNeighbor) {
            hexNeighbor.tint = "0xe2e7e9";
        }
    });
    stage.render();
}

window.stage9 = () => {
    let stage = newStage(400, 350);
    stage.selected = {}
    stage.hexes = [];
    stage.points = [];
    stage.points[-3] = [0, 1, 2, 3];
    stage.points[-2] = [-1, 0, 1, 2, 3];
    stage.points[-1] = [-2, -1, 0, 1, 2, 3];
    stage.points[0] = [-3, -2, -1, 0, 1, 2, 3];
    stage.points[1] = [-3, -2, -1, 0, 1, 2];
    stage.points[2] = [-3, -2, -1, 0, 1];
    stage.points[3] = [-3, -2, -1, 0];
    for (let x = -3; x<= 3; x++) {
        let points = stage.points[x];
        points.forEach((y) => {
            let hex = getXYHexagon({x:100, y:90}, x, y, POINTY, AXIAL);
            hex.addMessage(`${x},${y}`);
            hex.mouseover = (mouseData) => {
                stage.selectedHex = hex;
                reRenderWithDirections(stage, axial_directions);
            }
            stage.hexes.push(hex);
            stage.addChild(hex);
        });
    }
    stage.render();
    title.innerHTML = "Pointy Top Axial Grid<span>Hover Neighbours</span>";
}

window.stage10 = () => {
    let stage = newStage(400, 350);
    stage.selected = {}
    stage.hexes = [];
    stage.points = [];
    stage.points[-3] = [0, 1, 2, 3];
    stage.points[-2] = [-1, 0, 1, 2, 3];
    stage.points[-1] = [-2, -1, 0, 1, 2, 3];
    stage.points[0] = [-3, -2, -1, 0, 1, 2, 3];
    stage.points[1] = [-3, -2, -1, 0, 1, 2];
    stage.points[2] = [-3, -2, -1, 0, 1];
    stage.points[3] = [-3, -2, -1, 0];
    for (let x = -3; x<= 3; x++) {
        let points = stage.points[x];
        points.forEach((y) => {
            let hex = getXYHexagon({x:100, y:90}, x, y, POINTY, AXIAL);
            hex.addMessage(`${x},${y}`);
            hex.mouseover = (mouseData) => {
                stage.selectedHex = hex;
                reRenderWithDirections(stage, axial_diagonals);
            }
            stage.hexes.push(hex);
            stage.addChild(hex);
        });
    }
    stage.render();
    title.innerHTML = "Pointy Top Axial Grid<span>Hover Diagonals</span>";
}

const reRenderWithDistance = (stage) => {
    let selectedHex = stage.selectedHex;
    stage.hexes.forEach((hex) => {
        hex.tint = "0xFFFFFF";
        let xMsg = hex.index.x;
        let yMsg = hex.index.y;
        if (hex.distance === selectedHex.distance) {
            hex.tint = "0xe2e7e9";
        }
        hex.addMessage(`${xMsg},${yMsg}`);
    })
    selectedHex.addMessage(`<xb>${selectedHex.index.x}</xb>,<yb>${selectedHex.index.y}</yb>`);
    selectedHex.tint = "0xc3d7d0";
    stage.render();
}

const reRenderWithDistanceClick = (stage) => {
    let selectedHex = stage.selectedHex;
    let selectedDist = (Math.abs(selectedHex.index.y - stage.centerY) + Math.abs(selectedHex.index.y + selectedHex.index.x - stage.centerY - stage.centerX) + Math.abs(selectedHex.index.x - stage.centerX)) / 2
    stage.hexes.forEach((hex) => {
        hex.tint = "0xFFFFFF";
        let xMsg = hex.index.x;
        let yMsg = hex.index.y;
        let hexDistance = (Math.abs(hex.index.y - stage.centerY) + Math.abs(hex.index.y + hex.index.x - stage.centerY - stage.centerX) + Math.abs(hex.index.x - stage.centerX)) / 2
        if (hexDistance === selectedDist) {
            hex.tint = "0xe2e7e9";
        }
        hex.addMessage(`${xMsg},${yMsg}`);
    })
    selectedHex.addMessage(`<xb>${selectedHex.index.x}</xb>,<yb>${selectedHex.index.y}</yb>`);
    selectedHex.tint = "0xc3d7d0";
    stage.render();
}

window.stage11 = () => {
    let stage = newStage(500, 450);
    stage.selected = {}
    stage.hexes = [];
    stage.points = [];
    stage.points[-4] = [0, 1, 2, 3, 4];
    stage.points[-3] = [-1, 0, 1, 2, 3, 4];
    stage.points[-2] = [-2, -1, 0, 1, 2, 3, 4];
    stage.points[-1] = [-3, -2, -1, 0, 1, 2, 3, 4];
    stage.points[0] = [-4, -3, -2, -1, 0, 1, 2, 3, 4];
    stage.points[1] = [-4, -3, -2, -1, 0, 1, 2, 3];
    stage.points[2] = [-4, -3, -2, -1, 0, 1, 2];
    stage.points[3] = [-4, -3, -2, -1, 0, 1];
    stage.points[4] = [-4, -3, -2, -1, 0];
    for (let x = -4; x<= 4; x++) {
        let points = stage.points[x];
        points.forEach((y) => {
            let hex = getXYHexagon({x:130, y:110}, x, y, POINTY, AXIAL);
            hex.addMessage(`${x},${y}`);
            hex.distance = (Math.abs(y) + Math.abs(y + x) + Math.abs(x)) / 2
            hex.mouseover = (mouseData) => {
                stage.selectedHex = hex;
                reRenderWithDistance(stage);
            }
            stage.hexes.push(hex);
            stage.addChild(hex);
        });
    }
    stage.render();
    title.innerHTML = "Pointy Top Axial Grid<span>Hover Equal Distance</span>";
}

window.stage12 = () => {
    let stage = newStage(500, 450);
    stage.selected = {}
    stage.hexes = [];
    stage.points = [];
    stage.points[-4] = [0, 1, 2, 3, 4];
    stage.points[-3] = [-1, 0, 1, 2, 3, 4];
    stage.points[-2] = [-2, -1, 0, 1, 2, 3, 4];
    stage.points[-1] = [-3, -2, -1, 0, 1, 2, 3, 4];
    stage.points[0] = [-4, -3, -2, -1, 0, 1, 2, 3, 4];
    stage.points[1] = [-4, -3, -2, -1, 0, 1, 2, 3];
    stage.points[2] = [-4, -3, -2, -1, 0, 1, 2];
    stage.points[3] = [-4, -3, -2, -1, 0, 1];
    stage.points[4] = [-4, -3, -2, -1, 0];

    stage.centerX = 0;
    stage.centerY = 0;
    for (let x = -4; x<= 4; x++) {
        let points = stage.points[x];
        points.forEach((y) => {
            let hex = getXYHexagon({x:130, y:110}, x, y, POINTY, AXIAL);
            hex.addMessage(`${x},${y}`);
            hex.mouseover = (mouseData) => {
                stage.selectedHex = hex;
                reRenderWithDistanceClick(stage);
            }
            hex.click = () => {
                stage.centerX = hex.index.x;
                stage.centerY = hex.index.y;
                reRenderWithDistanceClick(stage);
            }
            stage.hexes.push(hex);
            stage.addChild(hex);
        });
    }
    stage.render();
    title.innerHTML = "Pointy Top Axial Grid<span>Click to Set Center</span>";
}
